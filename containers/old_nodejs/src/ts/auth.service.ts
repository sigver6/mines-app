import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import { LoginInfo } from './login.info'; 
import { ExecuteInfo } from './execute.info';

@Injectable()
export class AuthService {

  isLoggedIn: boolean = false;

  constructor (private http: Http){
  }


  // store the URL so we can redirect after logging in
  redirectUrl: string;

  login(loginInfo:LoginInfo) {
    let body = JSON.stringify( loginInfo );
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.isLoggedIn = true;
    var ret = this.http.post('https://mines.sigs.org/api/v1/sessions',body,options)
                       .map(res => res.json());
    return ret;
  }

  logout() {
    this.isLoggedIn = false;
  }


  private extractData(res: Response) {
    let body = res.json();
    // {execute:[code], token: [the token created by jwt from backend server]}
    return body || {};
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}
