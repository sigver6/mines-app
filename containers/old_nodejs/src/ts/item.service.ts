import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Item }           from './item';
import { Observable }     from 'rxjs/Observable';

@Injectable()
export class ItemService {

  

  private baseUrl = "https://mines.sigs.org/api/v1/";

  constructor (private http: Http) {
  }

  getItems (): Observable<Item[]> {
    let headers = new Headers({ 'Authorization' : localStorage.getItem('auth_token')}); 
    let options = new RequestOptions({headers: headers});

    return this.http.get(`${this.baseUrl}members`,options)
                    .map(response => response.json())
                    .catch(this.handleError);
  }

  addItem (name: string): Observable<Item> {
    let body = JSON.stringify({ name });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${this.baseUrl}/members`, body, options)
                    .map(res => res.json())
                    .catch(this.handleError);
  }
/*
  updatItem(item: Item){
    let body = JSON.stringify(item));
    return this.http.put(`${this.baseUrl}/members/${item.id}`,JSON.stringify(item))
                    .map(res => res.json())
                    .catch(this.handleError);
                     
  }

  deleteItem(itemId: number) {
    return this.http.delete(`${this.baseUrl}/members/${itemId}`)
                    .catch(this.handleError);
  }
*/

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

/*
  private itemesUrl = 'app/itemes.json'; // URL to JSON file
*/


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
