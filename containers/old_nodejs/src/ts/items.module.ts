import { NgModule }                from '@angular/core';
import { CommonModule }            from '@angular/common';
import { FormsModule }             from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { ItemListComponent } from './app.list.component';

import { ItemService } from './item.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    JsonpModule
  ],
  declarations: [
    ItemListComponent
  ],
  providers: [
    ItemService
  ]
})

export class ItemsModule{}
