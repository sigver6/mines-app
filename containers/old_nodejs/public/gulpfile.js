var gulp = require('gulp');
var sass = require('gulp-sass');

var typescript = require('gulp-typescript');
var webpack = require('gulp-webpack');

var fs = require('fs');
var gls = require('gulp-live-server');

var src = fs.createReadStream('/dev/random')
var dst = fs.createWriteStream('/dev/null')


var requireDir = require ('require-dir');
requireDir('./gulp',{rescure: true});

var config = require('./gulp/config');

gulp.task('typescript',function () {
    return gulp.src(config.ts.src)
               .pipe(typescript(config.ts.options))
               .js
               .pipe(gulp.dest(config.ts.dst));
});

gulp.task('webpack',['typescript'],function(){
    return gulp.src(config.webpack.entry)
               .pipe(webpack(config.webpack))
               .pipe(gulp.dest('./public/quartz/javascripts'));
});

gulp.task('watch',['server'], function(){
  gulp.watch('src/ts*.ts',['webpack']);
});

gulp.task('server',['webpack'],function(){
  var server = gls.static('public',5000);
  server.start();  
});

gulp.task('default',['watch']);
