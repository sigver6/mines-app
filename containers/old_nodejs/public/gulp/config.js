module.exports = {
  ts: {
        src: [
            './src/ts/*.ts',
            '!./node_modules/**'
        ],
        dst: './dest',
        options: {
          target: 'ES6',
          module: 'commonjs',
          moduleResolution: 'node',
          sourceMap: true,
          emitDecoratorMetadata: true,
          experimentalDecorators: true,
          removeComments: false,
          noImplicitAny: true,
          suppressImplicitAnyIndexErrors: true
        }
  }, 
  webpack: {
    entry: './dest/boot.js',
    output: {
      filename: 'bundle.js'
    },
    resolve: {
      extensions: ['', '.js']
    }
  }
};
