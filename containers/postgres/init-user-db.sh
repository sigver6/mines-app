#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
  CREATE ROLE jewel WITH LOGIN CREATEDB PASSWORD '7pel^vis';
EOSQL

