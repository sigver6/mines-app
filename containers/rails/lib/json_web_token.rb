class JsonWebToken
  class << self 

    def encode payload, exp = 2.hours.from.now
      payload[:exp] = exp.to_i
      JWT.encode payload 
    end

    def decode token
      rsa_private = OpenSSL::PKey::RSA.generate 2048
      rsa_public = rsa_private.public_key
      JWT.decode token, rsa_public, true, { :algorithm => 'RS256'}
    rescue
      nil
    end

  end
end
