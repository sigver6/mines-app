class CreateTravels < ActiveRecord::Migration[5.0]
  def change
    create_table :travels do |t|
      t.string :dest
      t.integer :num

      t.timestamps
    end
  end
end
