# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

=begin
                                        Table "public.accounts"
     Column      |            Type             |                       Modifiers                       
-----------------+-----------------------------+-------------------------------------------------------
 id              | integer                     | not null default nextval('accounts_id_seq'::regclass)
 email           | character varying           | 
 created_at      | timestamp without time zone | not null
 updated_at      | timestamp without time zone | not null
 admin           | boolean                     | 
 name            | character varying           | 
 password_digest | character varying           | 
 remember_digest | character varying           | 
Indexes:
    "accounts_pkey" PRIMARY KEY, btree (id)
=end

#account = Account.create(:email => 'stonickers@gmail.com', :name => 'sigver6', 
#                         :password_digest => Account.digest('sigsplanet'))
#account.save!

member = Member.create(:name => 'Renna')
member.save
member = Member.create(:name => 'Katie')
member.save

