class Account < ApplicationRecord
  has_secure_password
  attr_accessor :remember_token, :activation_token
  before_create :create_activation_digest
  validates :email, presence: true, length: { maximum: 128}, uniqueness:{case_sensitive: false}
  validates :password_digest, presence: true, length:{minimum:8}

  # 文字列のハッシュ値を返す
  def Account.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost) 
  end

  # ランダムなトークンを返す。（下のrememberで使うため。）
  def Account.new_token
    SecureRandom.urlsafe_base64
  end

  # 永続的セッションで使用するユーザをデータベースに記憶する。
  def remember
    self.remember_token = Account.new_token
    update_attribute(:remember_digest,Account.digest(remember_token))
  end

  #トークンがダイジェストと一致したらtrueを返す。
  def authenticated? ( remember_token )
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  # ユーザログインを破棄する。
  def forget
    update_attribute(:remember_digest, nil)
  end

  private
    # アカウント認証のトークンとダイジェストの発行
    def create_activation_digest
      self.activation_token  = Account.new_token
      self.activation_digest = Account.digest(activation_token)
    end

end
