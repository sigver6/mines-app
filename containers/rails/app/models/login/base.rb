class Login::Base
  include ActiveModel::Model

  attr_accessor :email, :password

  validates :email, presence: true
  validates :password, presence: true

  def calls
    puts "called"
  end

  def payload email 
  {
    exp: Time.now.to_i + 60 * 60,
    iat: Time.now.to_i,
    iss: 'mines.sigs.org',
    email: {
        email: email
    }
  }
  end

end
