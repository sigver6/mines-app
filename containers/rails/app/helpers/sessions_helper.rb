module SessionsHelper

  # ヘルパーに渡されたユーザーでログインする。
  def log_in(user_account)
    session[:user_id] = user_account.id
  end

  # ユーザーを永続的セッションに記憶する。
  def remember(user_account)
    user_account.remember
    cookies.permanent.signed[:user_id] = user_account.id
    cookies.permanent[:remember_token] = user_account.remember_token
  end

  def current_user?(user)
    user == current_user
  end 

  # ログインしている場合、そのユーザの情報を返す。 
  def current_user
      @current_user ||= Account.find_by(id: session[:user_id])
  end
 
  # 永続的セッションを破棄する
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # ERBの中で表示だし分けするためのモジュール
  def logged_in?
    !current_user.nil?
  end

  # セッションを破棄する。
  def helper_log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
end
