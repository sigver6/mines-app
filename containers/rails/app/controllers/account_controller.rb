class AccountController < ApplicationController

  def index
    personal = {'name' => 'Yamada', 'old' => '28'}

    respond_to do |format|   
      format.html
      format.json {render :json => personal}
    end

  end

  def show
  end 

  # ユーザー登録フォーム表示処理
  def signup
    @user_account = Account.new
  end

  # ユーザー登録処理
  def finish
    # account_paramsはコントラローラーの中で定義する。
    @user_account = Account.new(account_params) 
    if @user_account.save
      # @userと書くとuser_url といったURLの規約で書かれてるが
      # どうかけばよいかわからない。(nginxのリダイレクト条件に/railsが入るため)
      flash[:success]='User account registered.'
      redirect_to :controller => 'account', :action => 'index'
    else
      flash[:danger]='registratioin failed.'
      render 'signup'
    end
  end

  private
    
    def account_params
      params.require(:account).permit(:name,:email,:password,:password_confirmation)
    end

end
