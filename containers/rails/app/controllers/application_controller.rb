class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception ['rails/login']
  rescue_from ActiveRecord::RecordNotFound, with: :handle_404
  PRIVATE_KEY_PATH = '/root/.ssh/id_pk_rsa.pub'

  
  attr_reader :current_user

  def authenticate_request!
    logger.debug "called authenticate"
     
    unless request.headers['Authorization'].present? then
      render json: { errors: "Not Header" }, status: 403
    end

    token = request.headers['Authorization'].split(' ').last

    rsa_public = nil
    File.open(PRIVATE_KEY_PATH) do |f|
      rsa_public = OpenSSL::PKey::RSA.new(f)
    end    
    
    decoded = JWT.decode token, rsa_public, true, { :algorithm => 'RS256'}
   
    logger.debug decoded.inspect.to_s

    rescue JWT::VerificationError, JWT::DecodeError => e
      logger.debug{e.backtrace.join("\n")}
      render json: { errors: '403 Error'}, status: 403
  end


  def handle_404(exception = nil)
    logger.info "Rendering 404 with exception: #{exception.message}" if exception
    render json: { error: '404 Error' }, status: 404
  end

  private


  include SessionsHelper
end
