class SessionsController < ApplicationController

  def index
  end

  def create
    @user_account = Account.find(1)
    flash[:success] = "Welcome to the Sample App!"
    session[:user_id]='1'
    redirect_to :controller => 'account', :action => 'index'
  end

  def destroy
    session.delete(:user_id)
    @current_user = nil
    redirect_to :controller => 'sessions', :action => 'index'
  end

  def next
  end

end
