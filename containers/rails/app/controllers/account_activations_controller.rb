class AccountActivationsController < ApplicationController

  def edit
    flash[:success] = "Account activated."
    redirect_to :controller => 'account', :action => 'index'
  end

end
