require "#{Rails.root}/app/controllers/application_controller.rb"

module Api
  module V1
    class MembersController < ApplicationController
      before_action :set_member, only: [:show, :edit, :update, :destroy]
      before_action :authenticate_request!

      def index
        render json: Member.all
      end

      def show
        # テーブル結合で他のテーブルの値も出すんだろうな。
        render json: MemberResource.new(Member.find(params[:id]))
      end

      def new
        @member = Member.new
      end

      def edit
      end

      def create
        # 入力パラメータのValidationチェックは？ 
        @member = Member.new(member_params)
        if @member.save!
          render json: @member
        else
          render JSON.generate({"execute" => "900"})
        end
      end

      def update
        # 入力パラメータのValidationチェックは？
        # 競合が出た時に409エラーを返す必要がありそう。
        if @member.update!(member_params)
          data = {"execute" => "700", "status_code" => :ok }
        else
          # conflict -> 209
          data = {"execute" => "900"}
        end
        render json: JSON.generate(data)
      end

      def destroy
        @member.destroy
        # 削除されてる時は？404エラーにしたほうがよいか。
        data = {"execute" => "700", "status_code" => :no_content}
        render json: JSON.generate(data)
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_member
          @member = Member.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def member_params
          params.require(:member).permit(:name)
        end
    end
  end
end
