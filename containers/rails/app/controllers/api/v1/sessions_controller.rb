module Api
  module V1
    class SessionsController < ApplicationController
      protect_from_forgery :except => [:create,:destroy]

      def create
        exp = Time.now.to_i + 1 * 3600
        payload = {:email => params['email'] , :exp => exp }
        rsa_private = OpenSSL::PKey::RSA.new File.read '/root/.ssh/id_pk_rsa'
        token = JWT.encode payload, rsa_private, 'RS256'
        session[:user_id] = token
        render json: JSON.generate({"execute" => "900","token" => token})
      end

      def destroy
        session.delete(:user_id)
        @current_user = nil
        redirect_to :controller => 'sessions', :action => 'index'
      end

      def next
      end

      private
      

    end
  end
end
