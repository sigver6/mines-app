require 'jwt'

rsa = OpenSSL::PKey::RSA.generate 2048
File.open('/root/.ssh/id_pk_rsa','w') do |f|
  f.write(rsa)
end

public_key = rsa.public_key
File.open('/root/.ssh/id_pk_rsa.pub','w') do |f|
  f.write(public_key.export)
end

exp = Time.now.to_i + 4 * 3600
payload = {:data => 'test'}

token = JWT.encode payload, rsa, 'RS256'

puts token

puts "<><><><>"

decoded_token =  JWT.decode token, public_key, true, { :algorithm => 'RS256'}

puts decoded_token
