working_directory "/myapp"
worker_processes 2
pid "/myapp/tmp/pids/unicorn.pid"
listen 3000
listen "/var/run/appserver/unicorn.sock"
#stdout_path "/myapp/log/unicorn.stdout.log"
#stderr_path "/myapp/log/unicorn.stderr.log"
