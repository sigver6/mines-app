Rails.application.routes.draw do

  namespace :api, format: 'json' do
    namespace :v1 do
      resources :members
      resource :sessions, only: [:create, :destroy]
    end
  end
  resources :account_activations, only: [:edit]
  # For details on the DSL available within this file, 
  # see http://guides.rubyonrails.org/routing.html


  get    'rails/login'    => 'sessions#index'
  post   'rails/login'    => 'sessions#create'
  delete 'rails/logout'   => 'sessions#destroy'
  get    'rails/index'    => 'account#index'
  get    'rails/register' => 'account#signup'
  post   'rails/register' => 'account#finish'
  get    'rails/contact'  => 'menu#contact' 
  get    'rails/members'  => 'members#index'
  post   'rails/new'      => 'members#new'
  
  # Routing Errorで500エラーを返さず、404エラーで返すためにこんなことしてる。
  match "*path" => "application#handle_404", via: :all

end
