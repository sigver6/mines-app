#!/bin/sh
docker ps --all | awk 'BRGIN {FS="\t"} {print $1}' | grep -v CONTAINER | xargs docker rm -f
docker images | awk 'BRGIN {FS="\t"} {print $3}' | grep -v REPOSITORY | xargs docker rmi -f
