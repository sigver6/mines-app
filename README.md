# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

# Name

Mines

# Overview

This project contains two practice applications.
Single page application.
API using Ruby on Rails.

# Description

Under construction

# Versions

* nginx 1.11
* ruby 2.3.1
* node 6.3.0
* postgres 9.5
* redis 3.2

# Other

This project consists of five containers by docker-compose.

