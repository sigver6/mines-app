#!/bin/sh

rm dbdata
ln -s ${BASEDIR}data/postgres dbdata

cd ${BASEDIR}mines/containers
rm nodejs
ln -s ${BASEDIR}mines-nodejs nodejs
cd ${BASEDIR}mines

UNICORN_PIDFL_PATH=${BASEDIR}mines/containers/rails/tmp/pids/unicorn.pid

if [ -f "${UNICORN_PIDFL_PATH}" ];
then
  rm ${UNICORN_PIDFL_PATH}
fi

cp ./org.vimrc ${BASEDIR}mines/containers/nodejs/org.vimrc

docker-compose build
#docker-compose run rails-unicorn rails db:create RAILS_ENV=staging
#docker-compose run rails-unicorn rails db:migrate RAILS_ENV=staging
#docker-compose run rails-unicorn rails db:seed RAILS_ENV=staging
#docker-compose up
#docker ps

rm ${BASEDIR}mines/containers/nodejs/org.vimrc

# migration
#docker-compose run rails-unicorn rails generate migration add_3columns password:string,remember:string,admin:boolean

# check db 
# docker-compose run rails-unicorn psql topaz -h db -U postgres

#docker-compose run rails-unicorn rails generate migration add_admin_to_accounts admin:boolean

#docker-compose run rails-unicorn kill -HUP `cat tmp/pids/unicorn.pid`
